import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import { config } from 'dotenv'
import * as path from 'path'

chai.use(chaiAsPromised)
config({ path: path.resolve(process.cwd(), 'env.local') })
process.env.NODE_ENV = 'test'

process
  .on('uncaughtException', (err) => console.log(err)) // eslint-disable-line no-console
  .on('unhandledRejection', (err) => console.log(err)) // eslint-disable-line no-console
