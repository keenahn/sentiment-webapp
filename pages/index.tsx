import Head from 'next/head'
import React, { useState } from 'react'

import type { TSentiment } from '../src/config/constants'
import { SENTIMENT } from '../src/config/constants'
import { trpc } from '../src/utils/trpc'

const CompletionsList = (props: {
  completions: { query: string; completion: string; id: string }[]
}) => (
  <ul>
    {props.completions &&
      props.completions.map((c) => (
        <li key={c.id} id={`completion-${c.id}`} className={c.completion}>
          <>
            {c.query} - {c.completion}
          </>
        </li>
      ))}
  </ul>
)

const NUM_BACKGROUNDS_PER_SENTIMENT = 4

const randomBackgroundClassName = (sent?: TSentiment) =>
  `${sent ?? SENTIMENT.NEUTRAL}-${Math.floor(Math.random() * NUM_BACKGROUNDS_PER_SENTIMENT) + 1}`

/**
 * The index page
 */
const IndexPage = () => {
  const [phrase, setPhrase] = useState('')
  const [isLoading, setLoading] = useState(false)
  const [className, setClassName] = useState('neutral-1')

  const { data, refetch } = trpc.sentiment.useQuery(
    { phrase },
    {
      refetchOnWindowFocus: false,
      enabled: false, // disable this query from automatically running,
      retry: false,
    }
  )

  const { data: completions, refetch: refetchCompletions } = trpc.allCompletions.useQuery(
    { take: 10 },
    {
      refetchOnWindowFocus: false,
      enabled: true, // disable this query from automatically running,
      retry: false,
    }
  )

  const handleSubmit = async () => {
    setLoading(true)
    await refetch().then((res) =>
      setClassName(randomBackgroundClassName(res?.data?.sentiment as TSentiment))
    )
    setLoading(false)

    refetchCompletions()
  }

  const handleEnter = (e: React.KeyboardEvent) => (e.key === 'Enter' ? handleSubmit() : undefined)

  return (
    <>
      <Head>
        <title>Sentiment Analysis</title>
      </Head>
      <div id='wrap' className={className}>
        <p>Type a phrase and hit enter to get the sentiment</p>
        <input
          type='text'
          id='phrase'
          onChange={(e) => setPhrase(e.target.value)}
          onKeyPress={handleEnter}
        />
        <h2 id='result-title'>Result</h2>
        <pre id='result'>
          {data ? JSON.stringify(data, null, 2) : isLoading ? 'Loading...' : ''}
        </pre>

        <h2>10 most recent</h2>
        {completions && <CompletionsList completions={completions} />}
      </div>
    </>
  )
}

export default IndexPage
