# Sentiment

## Setup

```bash
git clone git@gitlab.com:keenahn/sentiment-webapp.git
```

Add `OPENAI_API_KEY` to `.env`

## Run

### First time

For the first run ever, you'll need to set up the database.

```bash
# start the postgres server
docker-compose up postgres

# In a separate terminal, run prisma migrate, which will set up the db
DATABASE_URL="postgresql://postgres:postgres@localhost:5432/sentiment?schema=public" npx prisma migrate deploy

# Shut down the postgres with ctrl+c
```

### Any other time

To build and run:
```bash
docker-compose up
```

When using the app, if you get a 401 error, you probably forgot to add an `OPENAI_API_KEY` to your local `.env` file. In a real production environment, this would be an actual environment variable which would be available to the docker container.


## Architecture

This is a [nextjs](https://nextjs.org) app, using [trpc](https://trpc.io) for easily defining API endpoints, and [prisma](https://www.prisma.io/) as the ORM to interface with postgresql.

Most of the interesting code will be in `src/server`

In `pages` you'll see the `.tsx` files with the React components

The database schema can be found in `/prisma/schema.prisma`. You'll see there are just two tables, one for storing completions and one for storing openAI configurations. The second one is not strictly necessary for completing the assignment, but in a real world situation you would want to record exactly what configuration you used, in case you change the engine, temperature, top_p, etc.

We also record the full prompt that was sent to openAI as well as just the phrase we're trying to get the sentiment for. I do this again for auditing and if we ever want to change the full prompt, we can easily do so and compare accuracy.

## Known limitations

1. Nextjs comes with its own minimal server for doing server side rendering, serving the html and js, exposing API endpoints, and doing the actual backend work of the API. Nextjs is great for prototyping because of this. It takes care of a lot of boilerplate by using smart defaults, and by also packaging the frontend and backend together. Nextjs is also a great fit for serverless architecture because of this all-in-one approach.

However, in a real production app:

- we might have multiple frontends that all need to talk to a unified API service
- the frontend might need to scale more than the backend
- we will need to limit the number of concurrent connections to the database

Once we're out of the prototype phase, it would make sense to create a new API service (I would still use typescript/node for this) that the nextjs frontend can talk to. Nextjs provides an easy way to use your own server, so this is not a problem.

To solve the concurrent connections issue, I would cluster the databases and/or add a load balancer in front of them.

2. The gif background images are being served from the same server as the html. In a real production app these would be on edge nodes of a CDN (if not all the frontend).

3. There are no automated tests for this app. As the app really just glues together a few APIs, there was not much business logic to test. That being said, it would be useful to create some integration tests in the future, or maybe use some mock responses from the OpenAI API to do some basic parsing tests. I could write these easily but it would just take a bit more time.

4. The css for the entire SPA is stored in `public/index.css`. In a more complex app, we might want to use a component library and styled components.

Thanks for reading :)

