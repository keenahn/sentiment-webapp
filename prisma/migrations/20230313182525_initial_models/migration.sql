-- Add uuid

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- CreateTable
CREATE TABLE "openai_configs" (
    "id" UUID NOT NULL DEFAULT uuid_generate_v4(),
    "mode" TEXT NOT NULL DEFAULT 'complete',
    "model" TEXT NOT NULL DEFAULT 'text-davinci-003',
    "temperature" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "max_tokens" INTEGER NOT NULL DEFAULT 256,
    "top_p" INTEGER,
    "n" INTEGER,
    "stop" TEXT,
    "presence_penalty" DOUBLE PRECISION,
    "frequency_penalty" DOUBLE PRECISION,
    "best_of" INTEGER,
    "logit_bias" JSONB,
    "user" TEXT,
    "created_at" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "openai_configs_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "completions" (
    "id" UUID NOT NULL DEFAULT uuid_generate_v4(),
    "openai_config_id" UUID NOT NULL,
    "raw" TEXT NOT NULL,
    "query" TEXT NOT NULL,
    "completion" TEXT NOT NULL,
    "created_at" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "completions_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "openai_configs_created_at_idx" ON "openai_configs"("created_at");

-- CreateIndex
CREATE INDEX "openai_configs_updated_at_idx" ON "openai_configs"("updated_at");

-- CreateIndex
CREATE INDEX "completions_created_at_idx" ON "completions"("created_at");

-- CreateIndex
CREATE INDEX "completions_updated_at_idx" ON "completions"("updated_at");

-- CreateIndex
CREATE UNIQUE INDEX "completions_query_key" ON "completions"("query");

-- AddForeignKey
ALTER TABLE "completions" ADD CONSTRAINT "completions_openai_config_id_fkey" FOREIGN KEY ("openai_config_id") REFERENCES "openai_configs"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
