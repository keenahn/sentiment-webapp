# Dockerfile for main nextjs app
FROM node:18-alpine AS base

# Install dependencies only when needed
FROM base AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app

# Install dependencies
COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./
COPY prisma ./prisma/
RUN yarn global add pnpm && pnpm i --frozen-lockfile


# Rebuild the source code only when needed
FROM base AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
# COPY --from=deps /app/node_modules/.prisma/client ./node_modules/.prisma/client
COPY . .

# Copy Prisma's generated files. Needs to come after installing deps, otherwise `node_modules` is overwritten.
# Added "linux-musl" as binaryTarget in schema.prisma due to us running it in alpine.


# Cleanup, remove any generated binaries we don't need
RUN find ./node_modules/.prisma/client | grep so.node | grep -v linux | xargs rm -f
RUN find ./node_modules/.prisma/client | grep query-engine | xargs rm -f

RUN npm run build

# Production image, copy all the files and run next
FROM base AS runner
WORKDIR /app

ENV NODE_ENV production

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

# COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public/
# COPY --from=builder /app/package.json ./package.json

# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=builder --chown=nextjs:nodejs /app/.next/standalone ./
COPY --from=builder --chown=nextjs:nodejs /app/.next/static ./.next/static/
COPY --from=deps /app/node_modules/.prisma/client ./node_modules/.prisma/client

USER nextjs

EXPOSE 3000

ENV PORT 3000

CMD ["node", "server.js"]
