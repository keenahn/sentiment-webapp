import { httpBatchLink } from '@trpc/client'
import { createTRPCNext } from '@trpc/next'

import { TRPC_BASE_URL } from '../config/env'
import type { AppRouter } from '../server/routers/_app'

/**
 * Get the Base URL for trpc. The browser should use relative path, otherwise use the env var
 */
const getBaseUrl = () => (typeof window !== 'undefined' ? '' : TRPC_BASE_URL)

// This comes from https://trpc.io/docs/nextjs
export const trpc = createTRPCNext<AppRouter>({
  config({ ctx }) {
    return {
      links: [
        httpBatchLink({
          /**
           * If you want to use SSR, you need to use the server's full URL
           * @link https://trpc.io/docs/ssr
           **/
          url: `${getBaseUrl()}/api/trpc`,
        }),
      ],
      /**
       * @link https://tanstack.com/query/v4/docs/reference/QueryClient
       **/
      // queryClientConfig: { defaultOptions: { queries: { staleTime: 60 } } },
    }
  },
  /**
   * @link https://trpc.io/docs/ssr
   **/
  ssr: false,
})
