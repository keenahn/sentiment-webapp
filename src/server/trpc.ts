import { initTRPC } from '@trpc/server'

const t = initTRPC.create()

const { procedure, router } = t

export { procedure, router }
