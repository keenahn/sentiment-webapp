import { prisma } from '../../config/prisma'

/**
 * Create a Completion
 */
const create = ({
  openAIConfigId,
  raw,
  query,
  completion,
}: {
  openAIConfigId: string
  raw: string
  query: string
  completion: string
}) =>
  prisma.completion.create({
    data: {
      openAIConfig: { connect: { id: openAIConfigId } },
      raw,
      query,
      completion,
    },
  })

/**
 * Find a Completion by id
 */
const findById = (id: string) => prisma.completion.findUnique({ where: { id } })

/**
 * Returns the most recent completions (by default, 10)
 */
const findMostRecent = ({ skip, take = 10 }: { skip?: number; take?: number }) =>
  prisma.completion.findMany({ skip, take, orderBy: { createdAt: 'desc' } })

/**
 * The Completion model for our database
 */
const Completion = { create, findById, findMostRecent }

export { Completion }
