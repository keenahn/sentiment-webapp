import { z } from 'zod'

import { getSentimentAndSave } from '../handlers/sentiment'
import { Completion } from '../models/completion'
import { procedure, router } from '../trpc'

export const appRouter = router({
  sentiment: procedure
    .input(z.object({ phrase: z.string() }))
    .query(({ input: { phrase } }) => getSentimentAndSave({ phrase })),
  allCompletions: procedure
    .input(z.object({ skip: z.number().optional(), take: z.number().optional() }))
    .query(({ input: { skip, take } }) =>
      Completion.findMostRecent({ skip: skip ?? undefined, take: take ?? undefined })
    ),
})

export type AppRouter = typeof appRouter
