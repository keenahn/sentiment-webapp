import { DEFAULT_CONFIG, openai } from '../../config/openai'

const SENTIMENT_PROMPT_PRE = `Decide whether a statement's sentiment is positive, neutral, or negative.\n\nStatement: `
const SENTIMENT_PROMPT_POST = `Sentiment: `

type TCreateCompletionRequest = ArgumentTypes<typeof openai.createCompletion>[0]

/**
 * Creates a prompt that will get openAI to return the sentiment of a given phrase
 */
const sentimentPrompt = (phrase: string) =>
  `${SENTIMENT_PROMPT_PRE} ${phrase}\n${SENTIMENT_PROMPT_POST}`

/**
 * Retrieves the sentiment value from OpenAI for the given phrase
 */
const getSentiment = async ({
  phrase,
  config = DEFAULT_CONFIG,
}: {
  phrase: string
  config?: TCreateCompletionRequest
}) => {
  const prompt = sentimentPrompt(phrase)
  const res = await openai.createCompletion({
    ...config,
    prompt,
  })
  const sentiment = res.data.choices[0].text?.trim()
  if (!sentiment)
    throw new Error(`Something wacky happened with OpenAI: ${prompt}, ${JSON.stringify(config)}`)
  return { phrase, prompt, config, sentiment: sentiment.toLowerCase() }
}

export { getSentiment, sentimentPrompt }
