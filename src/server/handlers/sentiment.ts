import { DEFAULT_CONFIG_ID } from '../../config/openai'
import { getSentiment } from '../helpers/openai'
import { Completion } from '../models/completion'

/**
 * Gets the sentiment for a phrase, saves it to the database, and returns the relevant data
 */
const getSentimentAndSave = async ({ phrase }: { phrase: string }) => {
  const { prompt, sentiment } = await getSentiment({ phrase })
  const completion = await Completion.create({
    openAIConfigId: DEFAULT_CONFIG_ID,
    raw: prompt,
    query: phrase,
    completion: sentiment,
  })
  return { phrase, sentiment, completionId: completion.id }
}

export { getSentimentAndSave }
