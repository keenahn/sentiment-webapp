import { NODE_ENV, PRISMA_LOGGING } from './env'
import * as PrismaAll from '.prisma/client'
import { PrismaClient } from '.prisma/client'

/**
 * from: https://www.prisma.io/docs/guides/performance-and-optimization/connection-management#prevent-hot-reloading-from-creating-new-instances-of-prismaclient
 * Prevents a new PrismaClient from being created whenever hot reloading happens by storing in a global variable
 */
interface CustomNodeJsGlobal extends NodeJS.Global {
  prisma: PrismaClient
}

declare const global: CustomNodeJsGlobal

let instance: PrismaClient

const ProxyPrisma = new Proxy(PrismaClient, {
  construct(target, args) {
    if (instance) return instance
    instance = instance || new target(...args)
    return instance
  },
})

const prisma =
  global.prisma ||
  new ProxyPrisma({
    ...(PRISMA_LOGGING
      ? {
          log: [
            'info',
            `warn`,
            `error`,
            {
              emit: 'event',
              level: 'query',
            },
          ],
          errorFormat: 'pretty',
        }
      : {}),
  })

if (NODE_ENV === 'development') global.prisma = prisma

if (PRISMA_LOGGING) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  prisma.$on('query' as any, async (e: any) => {
    // eslint-disable-next-line no-console
    console.debug(`${e.query}`, e.params)
  })
}

export { prisma, PrismaAll }
