import * as env from 'env-var'

export const PORT = env.get('PORT').default('3000').asInt()
export const NODE_ENV = env.get('NODE_ENV').default('development').asString()
export const OPENAI_API_KEY = env.get('OPENAI_API_KEY').asString()
export const PRISMA_LOGGING = env.get('PRISMA_LOGGING').default('false').asBool()
export const TRPC_BASE_URL = env.get('TRPC_BASE_URL').default(`http://localhost:${PORT}`).asString()
