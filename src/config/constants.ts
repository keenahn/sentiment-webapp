import { values } from 'lodash/fp'

const POSITIVE = 'positive'
const NEUTRAL = 'neutral'
const NEGATIVE = 'negative'

const SENTIMENT = {
  POSITIVE,
  NEUTRAL,
  NEGATIVE,
} as const

const _SENTIMENT = values(SENTIMENT)

type TSentiment = (typeof _SENTIMENT)[number]

export { SENTIMENT }
export type { TSentiment }
