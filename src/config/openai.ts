import type { CreateCompletionRequest } from 'openai'
import { Configuration, OpenAIApi } from 'openai'

import { OPENAI_API_KEY } from './env'

const openai = new OpenAIApi(
  new Configuration({
    apiKey: OPENAI_API_KEY,
  })
)

// see prisma/migrations/20230313184453_add_default_config
const DEFAULT_CONFIG_ID = '340046e1-44d2-4b76-a53f-bd05b641a235'
const DEFAULT_CONFIG: CreateCompletionRequest = {
  model: 'text-davinci-003',
  temperature: 0,
  max_tokens: 10,
  top_p: 1,
} as const

export { DEFAULT_CONFIG, DEFAULT_CONFIG_ID, openai }
